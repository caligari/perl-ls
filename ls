#!/usr/bin/env perl

# List files in PERL (based on GNU Directory Listing - http://bit.ly/Ye6XMz)
# version 0.1 by Rafa Couto for Glenn Grimes ;)


# ls [OPTION]... [FILE]...
#
# Options:
#
# -l: use a long listing format
#
# -a, --all: do not ignore entries starting with .
#
# -d, --directory: list  directory entries instead of contents, 
#         and do not derefer ence symbolic links
#
# -S: sort by file size
#
# -t: sort by modification time, newest first
#
# -u: sort by access time, newest first
#
# -U: do not sort; list entries in directory order
#
# -r, --reverse: reverse order while sorting
#
# -R, --recursive: list subdirectories recursively
#
# -s, --size: print size of each file, in blocks


# pragmas
use strict;
use warnings;

use Fcntl ':mode';

# parse the params (files or options)
my @files = ();
my @options = ();
my $eoo = 0; # End Of Options (--)
foreach my $param (@ARGV) {

    # test the param
    if (!$eoo) { 

        if ($param eq '--') {

            # end of options
            $eoo = 1;
            next;
        }

        if ($param =~ /^-/) {

            # it is an option
            push(@options, $param);
            next;
        }
    }

    # it is a file
    push(@files, $param);
}

# no files => list current directory
push(@files, '.') if (@files eq 0);

# features
my $features = { 
    long => opt_defined('-l'),
    recursive => opt_defined('-R|--recursive'),
    all_files => opt_defined('-a|--all'),
    almost_all => opt_defined('-A|--almost-all'),
    reverse => opt_defined('-r|--reverse'),
    block_size => opt_defined('-s|--size'),
    multiple_files => @files > 1
    };

# order options
if (opt_defined('-U')) {
    $features->{sorting} = 0; # do not sort
} elsif (opt_defined('-S')) {
    $features->{sorting} = 'size';
} elsif (opt_defined('-t')) {
    $features->{sorting} = 'mtime';
} elsif (opt_defined('-u')) {
    $features->{sorting} = 'atime';
} else {
    $features->{sorting} = 'alpha'; # default is alphabetically
}

# cascading options
$features->{multiple_files} = 1 if ($features->{recursive});

# here we go
list_entry($_) foreach (@files);



##### procedures #####


sub opt_defined {

    my ($opt) = @_;
    return (grep(/^($opt)$/, @options) > 0);
}


sub list_entry {

    my ($entry) = @_;

    if (-d $entry) {


        # list directory 
        my @entries = ();
        my $total = 0;
        opendir (DIR, $entry);
        while (my $e = readdir(DIR)) {

            # hidden files
            if ($e =~ /^\./) {

                # do not list implied . and ..
                next if ($features->{almost_all} && ($e eq '.' || $e eq '..'));

                # ignore entries starting with .
                next if (!$features->{all_files});
            }

            # stat file
            my $sub_entry = "$entry/$e";
            my @stat = lstat($sub_entry);
            push(@entries, [$sub_entry, $e, @stat]);
            $total = $total + $stat[12];

            # recursive listing
            if ($features->{recursive} && -d $sub_entry)
            {
                # add subdirectory
                push(@files, $sub_entry) unless ($e eq '.' || $e eq '..');
            }
        }
        closedir(DIR);

        # sort
        if ($features->{sorting} eq 'alpha') { # alphabetically
            @entries = sort {$a->[0] cmp $b->[0]} @entries;
        } elsif ($features->{sorting} eq 'size') { # by size
            @entries = sort {$b->[9] <=> $a->[9]} @entries;
        } elsif ($features->{sorting} eq 'atime') { # by access time
            @entries = sort {$b->[10] <=> $a->[10]} @entries;
        } elsif ($features->{sorting} eq 'mtime') { # by modification time
            @entries = sort {$b->[11] <=> $a->[11]} @entries;
        }

        # reverse
        @entries = reverse(@entries) if ($features->{reverse});

        # print 
        print("$entry:\n") if $features->{multiple_files};
        print("total $total\n") if $features->{long};
        print_entry($_) foreach @entries;

        # fix when listing in columns
        print "\n" if (!$features->{long});
    }
    else
    {
        # entry is not a directory
        print_entry([$entry, $entry, stat($entry)]);
    }
}


sub print_entry {

    my ($entry) = @_;

    if ($features->{block_size}) {

        # print size of each file in blocks
        print $entry->[14].' ';
    }

    if ($features->{long}) {

        # long listing 
        my $perm = get_permissions($entry->[4]);
        my $links = $entry->[5];
        my $user = get_user($entry->[6]);
        my $group = get_group($entry->[7]);
        my $size = $entry->[9];
        my $time = get_filetime($entry->[10], $entry->[11]);
        my $rname = get_realname($entry);
        printf "%s %2u %8s %8s %6u %s %s\n", 
            $perm, $links, $user, $group, $size, $time, $rname;
    }
    else {

        # in columns listing
        print $entry->[1]."\t";
    }
}


sub get_permissions {
    my ($mode) = @_;

    # File types
    my $type = '-';
    if (S_ISDIR($mode)) { $type = 'd' }
    elsif (S_ISLNK($mode)) { $type = 'l' }
    elsif (S_ISBLK($mode)) { $type = 'b' }
    elsif (S_ISCHR($mode)) { $type = 'c' }
    elsif (S_ISFIFO($mode)) { $type = 'p' }
    elsif (S_ISSOCK($mode)) { $type = 's' }

    # Permissions: read, write, execute, for user, group, others.
    $mode = S_IMODE($mode);
    my @perms = ('-','-','-','-','-','-','-','-','-');
    $perms[0] = 'r' if ($mode & S_IRUSR);
    $perms[1] = 'w' if ($mode & S_IWUSR);
    $perms[2] = 'x' if ($mode & S_IXUSR);
    $perms[3] = 'r' if ($mode & S_IRGRP);
    $perms[4] = 'w' if ($mode & S_IWGRP);
    $perms[5] = 'x' if ($mode & S_IXGRP);
    $perms[6] = 'r' if ($mode & S_IROTH);
    $perms[7] = 'w' if ($mode & S_IWOTH);
    $perms[8] = 'x' if ($mode & S_IXOTH);

    # Setuid/Setgid/Stickiness
    if ($mode & S_ISUID) {
        $perms[2] = ($perms[2] eq 'x' ? 's' : 'S');
    }
    if ($mode & S_ISGID) {
        $perms[5] = ($perms[5] eq 'x' ? 's' : 'S');
    }
    if ($mode & S_ISVTX) {
        $perms[8] = ($perms[8] eq 'x' ? 't' : 'T');
    }

    return $type.join('', @perms);
}


sub get_filetime {
    my ($atime, $mtime) = @_;

    my $time = ($features->{sorting} eq 'atime' ? $atime : $mtime);
    my ($sec,$min,$hour,$mday,$mon,$year,$wday) = localtime($time);
    return sprintf("%04u-%02u-%02u %02u:%02u", 
            $year + 1900, $mon + 1, $mday, $hour, $min);
}


sub get_user {
    my ($uid) = @_;
    my ($name) = getpwuid ($uid);
    return $name;
}


sub get_group {
    my ($gid) = @_;
    my ($name) = getgrgid($gid);
    return $name;
}


sub get_realname {
    my ($entry) = @_;

    # test if entry is a symlink
    if (S_ISLNK($entry->[4])) {

        # symbolic link
        return $entry->[1].' -> '.readlink($entry->[0]);
    }
    else {

        # regular entry
        return $entry->[1];
    }
}

# vim: ts=4:et:ai:
